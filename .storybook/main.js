const customConfig = require('../webpack.config.js');

module.exports = {
  stories: [
    '../src/**/**.stories.mdx',
    '../src/**/*.stories.@(js|jsx|ts|tsx)',
    '../src/**/**.stories.@(js|jsx|ts|tsx)',
  ],
  addons: [
    'storybook-addon-jsx',
    '@storybook/addon-links',
    '@storybook/addon-actions',
    {
      name: '@storybook/addon-essentials',
      options: {
        docs: true,
        controls: true,
      },
    },
  ],
  features: {
    modernInlineRender: true,
  },

  webpackFinal:async (config) => {
    delete config.resolve.alias['emotion-theming'];
    delete config.resolve.alias['@emotion/styled'];
    delete config.resolve.alias['@emotion/core'];
    return {
      ...config,
      module: {
        rules: customConfig.module.rules,
      },
      resolve: {
        extensions: customConfig.resolve.extensions,
        alias: customConfig.resolve.alias,
      },
    };
  },
};
