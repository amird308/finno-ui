import React from 'react';
import { create } from '@storybook/theming';
import ThemeProvider from '../src/components/ThemeProvider/ThemeProvider';
import { defaultTheme } from '../src/theme/defaultTheme';
import '../src/styles/fonts.css';

const theme = create({
  base: 'light',
  appBorderRadius: 5,
  fontBase: '"Open Sans", sans-serif',
  fontCode: 'monospace',
  textInverseColor: 'rgba(255,255,255,0.9)',
  barBg: '#F5F5F5',
  inputBorderRadius: 4
})

export const parameters = {
  actions: {
    theme,
    argTypesRegex: '^on[A-Z].*'
  },
  docs: {
    theme,
  },
  controls: {
    theme,
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/
    }
  }
}

const withMuiTheme = (Story: any) => {
  return (
    <ThemeProvider dir='rtl' theme={defaultTheme}>
      <Story />
    </ThemeProvider>
  )
}


export const decorators = [withMuiTheme];
