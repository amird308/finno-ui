import React from 'react';

const styles = {
  display: 'flex',
  flexDirection: 'column',
};
const Container = ({ children, styles: newStyles, direction = 'column' }: any) => (
  <div style={{ ...styles, flexDirection: direction, ...newStyles }}>{children}</div>
);

export default Container;
