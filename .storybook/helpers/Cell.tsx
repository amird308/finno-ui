import React from 'react';

const styles = {
  margin: '20px',
  display: 'flex',
  justifyContent: 'space-between',
  flexWrap: 'wrap',
  alignItems: 'center',
};

const Cell = ({ children, style }: any) => <div style={{ ...styles, ...style }}>{children}</div>;

export default Cell;
