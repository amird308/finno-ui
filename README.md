## Run storybook

`npm install` - to install all dependencies

`npm run storybook` or `npm run start` - to run storybook with local dev server

## Install as package

First install dependencies in your project:

`npm install react react-dom @material-ui/core @material-ui/icons @material-ui/lab immer lodash`

Then install ui package itself:

`npm install finno-ui`

## Usage in project

In your root level entry point e.g `App.jsx`:

```javascript
// first import dependencies from material-ui
import { StylesProvider, ThemeProvider, createMuiTheme } from '@material-ui/core/styles';

// then import default theme
import { defaultTheme } from '@finno-ui/ui/dist/theme';

// StylesProvider makes sure, that our custom created css
// is more specific than material-ui original ones.
<StylesProvider injectFirst>
  <ThemeProvider theme={createMuiTheme(defaultTheme)}>
    <App />
  </ThemeProvider>
</StylesProvider>;
```

Inside your app on any page/container e.g `Page.js`:

```javascript
import { Button } from 'finno-ui';

// ...
// in your react components
// ...
<Button>Test button</Button>;
```
