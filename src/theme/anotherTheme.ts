import { ThemeOptions } from '@mui/material/styles';

export const anotherTheme: ThemeOptions = {
  breakpoints: {
    values: {
      xl: 1920,
      lg: 1420,
      md: 960,
      sm: 600,
      xs: 0,
    },
  },
  palette: {
    primary: {
      main: '#0bb537',
      contrastText: '#ffffff',
    },
    background: {
      paper: '#f0f1f3',
      default: '#fafafa',
    },
    error: {
      main: '#f60202',
      contrastText: '#ffffff',
    },
    warning: {
      main: '#FFE522',
      contrastText: '#ffffff',
    },
    success: {
      main: '#0bb537',
      contrastText: '#0bb537',
    },
    info: {
      main: '#2196f3',
      contrastText: '#ffffff',
    },
    text: {
      primary: '#0d0d0d',
      secondary: '#0d0d0d',
    },
  },
  shape: {
    borderRadius: 6,
  },
  typography: {
    h1: {
      fontWeight: 700,
      fontSize: 36,
      lineHeight: 1.5,
      letterSpacing: 0,
    },
    h2: {
      fontWeight: 700,
      fontSize: 30,
      lineHeight: 1.2,
      letterSpacing: 0,
    },
    h3: {
      fontWeight: 700,
      fontSize: 28,
      lineHeight: 1.167,
      letterSpacing: 0,
    },
    h4: {
      fontWeight: 700,
      fontSize: 24,
      lineHeight: 1.235,
      letterSpacing: 0,
    },
    h5: {
      fontWeight: 700,
      fontSize: 22,
      lineHeight: 1.334,
      letterSpacing: 0,
    },
    h6: {
      fontWeight: 700,
      fontSize: 20,
      lineHeight: 1.6,
      letterSpacing: 0,
    },
    subtitle1: {
      fontWeight: 700,
      fontSize: 28,
      lineHeight: 1.75,
      letterSpacing: 0,
    },
    subtitle2: {
      fontWeight: 700,
      fontSize: 24,
      lineHeight: 1.57,
      letterSpacing: 0,
    },
    body1: {
      fontWeight: 400,
      fontSize: 18,
      lineHeight: 1.5,
      letterSpacing: 0,
    },
    body2: {
      fontWeight: 400,
      fontSize: 16,
      lineHeight: 1.75,
      letterSpacing: 0,
    },
    button: {
      textTransform: 'none' as 'none',
      lineHeight: 1,
    },
    fontFamily: 'IRANSansX',
  },
};
