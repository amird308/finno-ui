import { ThemeOptions } from '@mui/material/styles';

export const defaultTheme: ThemeOptions = {
  breakpoints: {
    values: {
      xl: 1920,
      lg: 1420,
      md: 960,
      sm: 600,
      xs: 0,
    },
  },
  palette: {
    primary: {
      main: '#0039cb',
      dark: '#00123b',
      contrastText: '#ffffff',
    },
    secondary: {
      main: '#002363',
      dark: '#00123b',
      contrastText: '#cecece',
    },
    background: {
      paper: '#f0f1f3',
      default: '#fafafa',
    },
    error: {
      main: '#d50000',
      contrastText: '#ffffff',
    },
    warning: {
      main: '#ffab00',
      contrastText: '#ffffff',
    },
    success: {
      main: '#64dd17',
      contrastText: '#0bb537',
    },
    info: {
      main: '#2196f3',
      contrastText: '#ffffff',
    },
    text: {
      primary: '#0d0d0d',
      secondary: '#0d0d0d',
    },
  },
  shape: {
    borderRadius: 6,
  },
  typography: {
    h1: {
      fontWeight: 700,
      fontSize: 36,
    },
    h2: {
      fontWeight: 700,
      fontSize: 30,
    },
    h3: {
      fontWeight: 700,
      fontSize: 28,
    },
    h4: {
      fontWeight: 700,
      fontSize: 24,
    },
    h5: {
      fontWeight: 700,
      fontSize: 22,
    },
    h6: {
      fontWeight: 700,
      fontSize: 16,
    },
    body1: {
      fontWeight: 400,
      fontSize: 16,
    },
    body2: {
      fontWeight: 700,
      fontSize: 14,
    },
    subtitle1: {
      fontWeight: 400,
      fontSize: 14,
    },
    subtitle2: {
      fontWeight: 400,
      fontSize: 12,
    },
    caption: {
      fontWeight: 400,
      fontSize: 10,
    },
    fontFamily: 'IRANSansX',
  },
};
