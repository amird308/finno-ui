import React, { useState } from 'react';
import makeStyles from '@mui/styles/makeStyles';
import MUIModal, { ModalProps } from '@mui/material/Modal';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { Theme } from '@mui/material/styles';

export const useModal = (autoOpen = false) => {
  const [isOpen, setIsOpen] = useState(autoOpen);
  const [data, setModalData] = useState(null);

  const open = (data?: any) => {
    if (data) {
      setModalData(data);
    }
    setIsOpen(true);
  };

  const close = () => {
    setModalData(null);
    setIsOpen(false);
  };

  return { isOpen, open, close, data };
};

const useStyles = makeStyles((theme: Theme) => ({
  paper: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    minWidth: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    transform: 'translate(-50%, -50%)',
    borderRadius: `${theme.shape.borderRadius}px ${theme.shape.borderRadius}px 0 0`,
    overflow: 'hidden',
    '&:focus': {
      outline: 'none',
    },
  },
  header: {
    backgroundColor: theme.palette.primary.main,
    padding: '14px 20px 12px',
    display: 'flex',
    justifyContent: 'space-between',
    color: theme.palette.background.paper,
    '& *': {
      fontSize: '16px',
      fontWeight: 600,
      lineHeight: '24px',
      textAlign: 'left',
    },
  },
  content: {
    padding: '30px 40px 40px',
  },
}));

export interface IProps extends ModalProps {
  children: React.ReactElement;
  title?: string;
  onClose?: () => void;
  className?: string;
}

export default function Modal({ children, title, className = '', onClose, ...rest }: IProps) {
  const classes = useStyles();

  return (
    <div>
      <MUIModal {...rest} onClose={onClose}>
        <div className={`${classes.paper} ${className}`}>
          <div className={classes.header}>
            <Typography>{title}</Typography>
            {onClose && (
              <IconButton onClick={onClose} size="small" color="inherit">
                <CloseIcon />
              </IconButton>
            )}
          </div>
          <div className={classes.content}>{children}</div>
        </div>
      </MUIModal>
    </div>
  );
}
