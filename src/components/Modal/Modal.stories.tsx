import React from 'react';
import Container from '@root/.storybook/helpers/Container';
import Cell from '@root/.storybook/helpers/Cell';

import Button from '../Button/Button';

import Modal, { useModal } from './Modal';

export default {
  component: Modal,
  title: 'Modal',
};

export const States = () => {
  const { isOpen, open, close } = useModal();

  return (
    <Container>
      <Cell>
        <Button onClick={open}>Modal</Button>
        <Modal open={isOpen} onClose={close} title="Title of a modal">
          <div>Content here</div>
        </Modal>
      </Cell>
    </Container>
  );
};
