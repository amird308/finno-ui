import React from 'react';
import { action } from '@storybook/addon-actions';
import Container from '@root/.storybook/helpers/Container';
import Cell from '@root/.storybook/helpers/Cell';

import Checkbox from './Checkbox';

export default {
  component: Checkbox,
  title: 'Checkbox',
};

export const States = () => {
  return (
    <Container>
      <Cell>
        <Checkbox onChange={action('checkbox change')} />
      </Cell>
      <Cell>
        <Checkbox color="secondary" label="Checkbox secondary" />
      </Cell>
      <Cell>
        <Checkbox
          errorMessage="Error message"
          label="Checkbox with label and error nad custom font size"
          fontSize={20}
        />
      </Cell>
      <Cell>
        <Checkbox
          defaultChecked
          alignItems="flex-start"
          label="I have read and understand the information on the use of my personal data explained in the Privacy Policy, and I agree to receive customised commercial communications from  Schneider Electric by email and other means."
        />
      </Cell>
      <Cell>
        <Checkbox disabled label="disabled" />
      </Cell>
    </Container>
  );
};
