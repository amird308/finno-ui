import React, { ReactElement } from 'react';
import makeStyles from '@mui/styles/makeStyles';
import MUICheckbox, { CheckboxProps } from '@mui/material/Checkbox';
import FormHelperText from '@mui/material/FormHelperText';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import SvgIcon from '@mui/material/SvgIcon';

import CheckboxEmpty from './icons/checkbox-empty.svg';
import CheckboxActive from './icons/checkbox-active.svg';
import { Theme } from '@mui/material/styles';

const useStyles = makeStyles<Theme, Partial<ICheckboxProps>>((theme: Theme) => ({
  root: {
    alignItems: ({ alignItems }) => alignItems,
    color: theme.palette.text.primary,
    userSelect: 'none',
    '& .MuiCheckbox-root': {
      margin: '-12px 0',
    },
    '& ~ .MuiFormHelperText-root': {
      paddingLeft: 30,
    },
  },
  label: {
    fontSize: ({ fontSize }) => (fontSize ? fontSize : 14),
  },
}));

export interface ICheckboxProps extends CheckboxProps {
  errorMessage?: string;
  label: string | number | ReactElement;
  className?: string;
  alignItems?: string;
  fontSize?: number | string;
  onChange?: (e: React.ChangeEvent<any>) => void;
}

function Checkbox(props: ICheckboxProps) {
  const { errorMessage, label, onChange, className, fontSize, alignItems, ...rest } = props;
  const classes = useStyles({ fontSize, alignItems });

  const checkboxProps: CheckboxProps = {
    icon: <SvgIcon component={CheckboxEmpty as any} viewBox="-4 -4 24 24" />,
    checkedIcon: <SvgIcon component={CheckboxActive as any} viewBox="-4 -4 24 24" />,
    ...rest,
  };

  return (
    <FormControl error={!!errorMessage} className={className}>
      <FormControlLabel
        classes={classes}
        onChange={onChange}
        control={<MUICheckbox {...checkboxProps} />}
        label={label}
      />
      {errorMessage && <FormHelperText>{errorMessage}</FormHelperText>}
    </FormControl>
  );
}

Checkbox.defaultProps = {
  color: 'primary',
  errorMessage: '',
  className: '',
  alignItems: 'center',
  fontSize: null,
  label: '',
  onChange: () => {},
};

export default Checkbox;
