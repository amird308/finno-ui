import React, { useState } from 'react';
import Container from '@root/.storybook/helpers/Container';
import Cell from '@root/.storybook/helpers/Cell';
import ThemeProvider from './ThemeProvider';
import { defaultTheme } from '@theme/defaultTheme';
import { anotherTheme } from '@theme/anotherTheme';
import Button from '../Button/Button';
import TextField from '../TextField/TextField';

export default {
  component: ThemeProvider,
  title: 'ThemeProvider',
};

export const States = () => {
  const themes = {
    defaultTheme: defaultTheme,
    anotherTheme: anotherTheme,
  };
  const [theme, setTheme] = useState<'defaultTheme' | 'anotherTheme'>('defaultTheme');
  const [dir, setDir] = useState<'ltr' | 'rtl'>('ltr');
  return (
    <Container>
      <ThemeProvider dir={dir} theme={themes[theme]}>
        <Cell>
          <Button
            variant="contained"
            onClick={() => setDir(dir === 'ltr' ? 'rtl' : 'ltr')}
            color="primary"
          >
            change dir
          </Button>
          <Button
            onClick={() => setTheme(theme === 'defaultTheme' ? 'anotherTheme' : 'defaultTheme')}
            color="primary"
          >
            change theme
          </Button>
        </Cell>
        <Cell>
          <Button variant="contained" color="primary">
            hi man
          </Button>
          <Button variant="outlined" color="primary">
            hi man
          </Button>
          <Button color="primary"> hi man </Button>
        </Cell>
        <Cell>
          <TextField sx={{ marginTop: 1 }} label="name" />
          <TextField sx={{ marginTop: 1 }} label="last name" />
        </Cell>
      </ThemeProvider>
    </Container>
  );
};
