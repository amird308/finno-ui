import React, { FC } from 'react';
import { defaultTheme } from '@root/src/theme/defaultTheme';
import {
  ThemeProvider as MuiThemeProvider,
  createTheme,
  ThemeOptions,
  StyledEngineProvider,
} from '@mui/material/styles';
import rtlPlugin from 'stylis-plugin-rtl';
import { CacheProvider } from '@emotion/react';
import createCache from '@emotion/cache';
import { CssBaseline } from '@mui/material';

const cacheRtl = createCache({
  key: 'muirtl',
  stylisPlugins: [rtlPlugin],
});

export interface IThemeProviderProps {
  theme: ThemeOptions;
  dir: 'ltr' | 'rtl';
}

const ThemeProvider: FC<IThemeProviderProps> = (props) => {
  const theme = createTheme(props.theme);
  theme.direction = props.dir;
  document.body.dir = props.dir;
  return (
    <StyledEngineProvider injectFirst>
      {theme.direction === 'rtl' ? (
        <CacheProvider value={cacheRtl}>
          <CssBaseline />
          <MuiThemeProvider theme={theme}>{props.children}</MuiThemeProvider>
        </CacheProvider>
      ) : (
        <>
          <CssBaseline />
          <MuiThemeProvider theme={theme}>{props.children}</MuiThemeProvider>
        </>
      )}
    </StyledEngineProvider>
  );
};

ThemeProvider.defaultProps = {
  theme: { ...defaultTheme },
  dir: 'ltr',
};

export default ThemeProvider;
