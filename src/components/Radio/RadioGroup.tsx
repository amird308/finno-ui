import React, { ReactElement } from 'react';
import MUIRadioGroup, { RadioGroupProps } from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import FormHelperText from '@mui/material/FormHelperText';
import Radio from './Radio';

export interface IRadioGroupItems {
  value: string;
  label: ReactElement | string;
  disabled?: boolean;
}

export interface IRadioGroupComponents {
  value: string;
  component: {
    icon?: any;
    title?: string;
    description?: string;
  };
}

export interface IRadioGroupProps extends RadioGroupProps {
  items?: IRadioGroupItems[];
  components?: IRadioGroupComponents[];
  withBorder?: boolean;
  isHorizonal?: boolean;
  withRadio?: boolean;
  isSmall?: boolean;
  width?: number;
  marginBottom?: number;
  error?: string;
}

function RadioGroup(props: IRadioGroupProps) {
  const {
    items,
    components,
    withRadio,
    width,
    withBorder,
    isHorizonal,
    isSmall,
    marginBottom,
    error,
    ...rest
  } = props;

  const radioGroupProps: RadioGroupProps = {
    ...rest,
  };

  if (items?.length) {
    return (
      <FormControl component="fieldset">
        <MUIRadioGroup {...radioGroupProps}>
          {items.map((item) => (
            <FormControlLabel
              key={item.value}
              value={item.value}
              control={<Radio />}
              label={item.label}
              className={item.value === props.value ? 'active' : ''}
              disabled={item.disabled}
            />
          ))}
          {error && <FormHelperText className="MUIRadioGroup-error">{error}</FormHelperText>}
        </MUIRadioGroup>
      </FormControl>
    );
  }

  if (components?.length) {
    return (
      <FormControl component="fieldset">
        <MUIRadioGroup {...radioGroupProps}>
          {components.map((item) => {
            const { component } = item;
            const { title, description, icon: Icon } = component;
            const isActive = item.value === props.value;

            const label = (
              <div className="MUIRadioGroup-container">
                <div className="MUIRadioGroup-text">
                  {Icon && <Icon className="MUIRadioGroup-Icon" />}
                  <div className="MUIRadioGroup-title">{title}</div>
                  <div className="MUIRadioGroup-description">{description}</div>
                </div>
                {isSmall && isActive && (
                  <CheckCircleOutlineIcon color="primary" className="MUIRadioGroup-checked" />
                )}
              </div>
            );
            return (
              <FormControlLabel
                key={item.value}
                value={item.value}
                control={<Radio />}
                label={label}
                className={isActive ? 'active' : ''}
              />
            );
          })}
        </MUIRadioGroup>
      </FormControl>
    );
  }

  return null;
}

RadioGroup.defaultProps = {
  withBorder: false,
  isHorizonal: false,
  withRadio: true,
  isSmall: false,
  width: null,
  items: [],
  components: [],
};

export default RadioGroup;
