import React from 'react';
import MUIRadio, { RadioProps } from '@mui/material/Radio';

export interface IRadioProps extends RadioProps {}

function Radio(props: IRadioProps) {
  const { ...rest } = props;

  const checkboxProps: RadioProps = {
    ...rest,
  };

  return <MUIRadio {...checkboxProps} />;
}

Radio.defaultProps = {
  color: 'primary',
};

export default Radio;
