import React from 'react';
import { action } from '@storybook/addon-actions';
import Container from '@root/.storybook/helpers/Container';
import Cell from '@root/.storybook/helpers/Cell';
import Radio from './Radio';
import RadioGroup from './RadioGroup';
import AccessibilityIcon from '@mui/icons-material/Accessibility';
import AirplanemodeActiveIcon from '@mui/icons-material/AirplanemodeActive';
import BrightnessAutoIcon from '@mui/icons-material/BrightnessAuto';

export default {
  component: Radio,
  title: 'Radio',
};

export const RadioState = () => {
  return (
    <Container>
      <Cell>
        <Radio onChange={action('checkbox change')} />
      </Cell>
    </Container>
  );
};

export const RadioGroupState = () => {
  const [value, setValue] = React.useState('');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue((event.target as HTMLInputElement).value);
  };

  const defaultProps = {
    value,
    name: 'radio-group-name',
    onChange: handleChange,
  };

  const items = [
    {
      value: 'Radio value 1',
      label: <span>Radio value 1 in span</span>,
    },
    {
      value: 'Radio value 2',
      label: 'Radio value 2',
    },
  ];

  const components = [
    {
      value: 'Radio value 1',
      component: {
        icon: AirplanemodeActiveIcon,
        title: 'Title 1',
        description: 'Description 1',
      },
    },
    {
      value: 'Radio value 2',
      component: {
        icon: AccessibilityIcon,
        title: 'Title 2',
        description: 'Description 2',
      },
    },
  ];

  const componentsNoIcons = [
    {
      value: 'Radio value 1',
      component: {
        title: 'Title 1',
        description: 'Description 1',
      },
    },
    {
      value: 'Radio value 2',
      component: {
        title: 'Title 2',
        description: 'Description 2',
      },
    },
  ];

  const componentsSmall = [
    {
      value: 'Radio value 1',
      component: {
        icon: BrightnessAutoIcon,
        title: 'Title 1',
      },
    },
    {
      value: 'Radio value 2',
      component: {
        icon: BrightnessAutoIcon,
        title: 'Title 2',
      },
    },
  ];

  return (
    <Container>
      <Cell>
        <RadioGroup {...defaultProps} withBorder width={300} items={items} />
        <RadioGroup {...defaultProps} withBorder isHorizonal items={items} />
      </Cell>
      <Cell>
        <RadioGroup {...defaultProps} items={items} />
        <RadioGroup {...defaultProps} isHorizonal items={items} />
      </Cell>
      <Cell>
        <RadioGroup
          {...defaultProps}
          withBorder
          width={300}
          withRadio={false}
          components={components}
        />
        <RadioGroup
          {...defaultProps}
          withBorder
          isHorizonal
          withRadio={false}
          components={components}
        />
      </Cell>
      <Cell>
        <RadioGroup {...defaultProps} withBorder width={300} components={componentsNoIcons} />
        <RadioGroup
          {...defaultProps}
          withBorder
          width={300}
          withRadio={false}
          components={componentsNoIcons}
        />
      </Cell>
      <Cell>
        <RadioGroup
          {...defaultProps}
          components={componentsSmall}
          isSmall
          withRadio={false}
          withBorder
          width={200}
        />
        <RadioGroup
          {...defaultProps}
          isHorizonal
          components={componentsSmall}
          isSmall
          withRadio={false}
          withBorder
          width={200}
        />
      </Cell>
    </Container>
  );
};
