import React, { useState } from 'react';
import MUITextField, { TextFieldProps } from '@mui/material/TextField';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import Tooltip from '../Tooltip/Tooltip';

export type TTextFieldProps = TextFieldProps & {
  infoText?: string;
};

function TextField(props: TTextFieldProps) {
  const { infoText, InputProps, ...rest } = props;
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const inputProps = {
    disableUnderline: true,
    endAdornment: InputProps?.endAdornment,
    type: props.type === 'password' ? (showPassword ? 'text' : 'password') : props.type,
  };

  if (infoText) {
    inputProps.endAdornment = <Tooltip title={infoText} />;
  } else if (props.type === 'password') {
    const Icon = showPassword ? Visibility : VisibilityOff;

    inputProps.endAdornment = <Icon onClick={handleClickShowPassword} />;
  }
  const textfieldProps = {
    InputProps: props.variant === 'filled' ? { ...InputProps, ...inputProps } : InputProps,
    ...rest,
  };

  return <MUITextField {...textfieldProps} />;
}

TextField.defaultProps = {
  variant: 'outlined',
  fullWidth: true,
  infoText: '',
};

export default TextField;
