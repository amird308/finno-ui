import React from 'react';
import { action } from '@storybook/addon-actions';
import Container from '@root/.storybook/helpers/Container';
import Cell from '@root/.storybook/helpers/Cell';
import TextField from './TextField';


export default {
  component: TextField,
  title: 'TextField',
};

export const States = () => {
  return (
    <Container>
      <Cell>
        <TextField label="Bottom border input" variant="standard" onChange={action('typing')} />
      </Cell>
      <Cell>
        <TextField label="Empty State" onChange={action('typing')} />
      </Cell>
      <Cell>
        <TextField label="Password" value="Password" type="password" />
      </Cell>
      <Cell>
        <TextField label="Active" value="State" />
      </Cell>
      <Cell>
        <TextField error label="Error" helperText="Error" value="State" />
      </Cell>
      <Cell>
        <TextField value="Focused" label="Active" autoFocus />
      </Cell>
      <Cell>
        <TextField placeholder="Type here…" label="Disabled" disabled />
      </Cell>
      <Cell>
        <TextField placeholder="Type here…" label="Multiline" multiline maxRows={4} />
      </Cell>
    </Container>
  );
};
