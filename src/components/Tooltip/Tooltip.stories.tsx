import React from 'react';
import Container from '@root/.storybook/helpers/Container';
import Cell from '@root/.storybook/helpers/Cell';

import Tooltip from './Tooltip';

export default {
  component: Tooltip,
  title: 'Tooltip',
};

export const States = () => {
  return (
    <Container>
      <Cell>
        <div>
          Info tooltip <Tooltip title="Simple tooltip" />
        </div>
      </Cell>
    </Container>
  );
};
