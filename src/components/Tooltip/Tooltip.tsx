import React, { useState } from 'react';
import MUITooltip, { TooltipProps } from '@mui/material/Tooltip';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import ClickAwayListener from '@mui/material/ClickAwayListener';

export interface ITooltipProps extends TooltipProps {
  iconSize: number;
  fontSize: number;
}

function Tooltip(props: ITooltipProps) {
  const { iconSize, fontSize, ...rest } = props;
  const [open, setOpen] = useState(false);

  const handleTooltipClose = () => {
    setOpen(false);
  };

  const handleTooltipOpen = () => {
    setOpen(true);
  };

  const tooltipProps = {
    open,
    onMouseEnter: handleTooltipOpen,
    onClose: handleTooltipClose,
    PopperProps: { disablePortal: true },
    ...rest,
  };

  return (
    <ClickAwayListener onClickAway={handleTooltipClose}>
      <MUITooltip {...tooltipProps}>
        <InfoOutlinedIcon onClick={handleTooltipOpen} />
      </MUITooltip>
    </ClickAwayListener>
  );
}

Tooltip.defaultProps = {
  children: null,
  placement: 'top',
  iconSize: null,
  fontSize: null,
};

export default Tooltip;
