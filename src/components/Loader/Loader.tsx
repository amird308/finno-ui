import React from 'react';
import makeStyles from '@mui/styles/makeStyles';
import { Theme } from '@mui/material/styles';

export interface ILoaderProps {
  className?: string;
}

const useStyles = makeStyles({
  wrapper: {
    margin: '10px auto',
    width: '25px',
    height: '25px',
    position: 'relative',
  },
});

const useDotStyles = makeStyles((theme: Theme) => ({
  dot: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    left: 0,
    top: 0,
    transform: ({ index }: { index: number }) => `rotate(${0 + index * 30}deg)`,

    '&::before': {
      content: '""',
      display: 'block',
      margin: '0 auto',
      width: '10%',
      height: '10%',
      backgroundColor: theme.palette.primary.main,
      borderRadius: '100%',
      animation: '$skCircleBounceDelay 1.2s infinite ease-in-out both',
      animationDelay: ({ index }: { index: number }) => `${-1.1 - 0.1 * (index - 1)}s`,
    },
  },
  '@keyframes skCircleBounceDelay': {
    '0%': {
      transform: 'scale(0)',
    },
    '80%': {
      transform: 'scale(0)',
    },
    '100%': {
      transform: 'scale(0)',
    },
    '40%': {
      transform: 'scale(1)',
    },
  },
}));

const Dot = ({ index }: { index: number }) => {
  const dotClasses = useDotStyles({ index });
  return <div className={dotClasses.dot} />;
};

const Loader: React.FC<ILoaderProps> = ({ className = '' }: ILoaderProps) => {
  const classes = useStyles();
  return (
    <div className={`${classes.wrapper} ${className}`}>
      {new Array(12).fill(undefined).map((_element, index) => (
        <Dot key={index} index={index} />
      ))}
    </div>
  );
};

Loader.defaultProps = {
  className: '',
};

export default Loader;
