import React from 'react';
import Container from '@root/.storybook/helpers/Container';
import Cell from '@root/.storybook/helpers/Cell';

import Loader from './Loader';
import makeStyles from '@mui/styles/makeStyles';
import { Theme } from '@mui/material/styles';

export default {
  component: Loader,
  title: 'Loader',
};
const useStyles = makeStyles((theme: Theme) => ({
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
}));

export const States = () => {
  const styles = useStyles();
  console.log(styles);
  return (
    <Container>
      <Cell className={styles.button}>
        <Loader />
      </Cell>
    </Container>
  );
};
