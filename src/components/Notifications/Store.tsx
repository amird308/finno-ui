import { AlertColor } from '@mui/material/Alert';

export type TIncomeNotification = {
  type: AlertColor;
  text: string;
};

export interface INotification extends TIncomeNotification {
  type: AlertColor;
  text: string;
  id: string;
}

interface IRegisterParameter {
  addNotification: (notification: TIncomeNotification) => void;
  removeNotification: (id: string) => void;
}

interface IStore {
  error(message: string): void;
  success(message: string): void;
  info(message: string): void;
  warning(message: string): void;
  addNotification(notification: TIncomeNotification): void;
  removeNotification(id: string): void;
  register(param: IRegisterParameter): void;
}

class Store implements IStore {
  constructor() {
    this.notifications = [];
    this.add = null;
    this.remove = null;
  }

  public notifications: INotification[];

  private add: ((notification: TIncomeNotification) => void) | null;
  private remove: ((id: string) => void) | null;

  public addNotification(notification: TIncomeNotification) {
    return this.add && this.add(notification);
  }

  public error(text: string) {
    return this.add && this.add({ type: 'error', text });
  }

  public success(text: string) {
    return this.add && this.add({ type: 'success', text });
  }

  public info(text: string) {
    return this.add && this.add({ type: 'info', text });
  }

  public warning(text: string) {
    return this.add && this.add({ type: 'warning', text });
  }

  public removeNotification(id: string) {
    return this.remove && this.remove(id);
  }

  public register({
    addNotification,
    removeNotification,
  }: {
    addNotification: (props: TIncomeNotification) => void;
    removeNotification: (id: string) => void;
  }) {
    this.add = addNotification;
    this.remove = removeNotification;
  }
}

export default new Store();
