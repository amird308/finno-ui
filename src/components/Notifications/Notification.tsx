import React from 'react';

import Snackbar from '@mui/material/Snackbar';

import { INotification } from './Store';
import { Alert } from '@mui/material';

interface INotificationProps {
  notification: INotification;
  removeNotification: (id: string) => void;
}

export default function Notification({ removeNotification, notification }: INotificationProps) {
  const handleSnackClose = (event?: any, reason?: string) => {
    if (reason === 'clickaway') {
      return false;
    }

    return removeNotification(notification.id);
  };
  return (
    <Snackbar
      anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
      message={notification.text}
      open
      autoHideDuration={4000}
      onClose={handleSnackClose}
    >
      <Alert onClose={handleSnackClose} severity={notification.type} sx={{ width: '100%' }}>
        {notification.text}
      </Alert>
    </Snackbar>
  );
}
