import { createContext } from 'react';
import { AlertColor } from '@mui/material/Alert';

import { INotification, TIncomeNotification } from './Store';

export type TTranslation = {
  [key in AlertColor]: string;
};

export enum EAction {
  add = 'add',
  remove = 'remove',
}

export type TState = { notifications: INotification[]; translations: TTranslation };
export type TActionType = EAction | string;
export type TAction = { type: TActionType; payload: TIncomeNotification | string };
export type TDispatch = (action: TAction) => void;

export const NotificationsStateContext = createContext<TState | undefined>(undefined);
export const NotificationsDispatchContext = createContext<TDispatch | undefined>(undefined);
