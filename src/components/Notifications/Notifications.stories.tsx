import React from 'react';
import { AlertColor } from '@mui/material/Alert';
import Container from '@root/.storybook/helpers/Container';
import Cell from '@root/.storybook/helpers/Cell';

import Button from '../Button/Button';

import Notifications from './Notifications';
import notification from './Store';

export default {
  component: Notifications,
  title: 'Notifications',
};

const translations = { error: 'Error!', info: 'Info!', warning: 'Warning!', success: 'Success!' };

export const States = () => {
  const addNotification = (type: AlertColor, message: string) => {
    notification[type](message);
  };

  return (
    <Container>
      <Cell>
        <Button
          onClick={() =>
            addNotification(
              'info',
              'We will be performing server upgrades on October 12th from 6 am - 8 am PST. Servise interruptions may occur.',
            )
          }
        >
          Info
        </Button>
      </Cell>
      <Cell>
        <Button
          onClick={() =>
            addNotification(
              'success',
              'Your account was successfully updated. You can start your happy dance',
            )
          }
        >
          Success
        </Button>
      </Cell>
      <Cell>
        <Button
          onClick={() =>
            addNotification(
              'error',
              'With our Charger you choose a modern charging solution. So individual that it has the right offer including installation for every requirement.',
            )
          }
        >
          Error
        </Button>
      </Cell>
      <Cell>
        <Button
          onClick={() =>
            addNotification('warning', 'Sory, something went wrong but we are looking into it')
          }
        >
          Warning
        </Button>
      </Cell>
      <Notifications translations={translations} />
    </Container>
  );
};
