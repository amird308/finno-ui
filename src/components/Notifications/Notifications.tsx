import React, { useEffect, useReducer, useContext } from 'react';
import { produce } from 'immer';
import findIndex from 'lodash/findIndex';
import makeStyles from '@mui/styles/makeStyles';

import store, { INotification, TIncomeNotification } from './Store';
import Notification from './Notification';
import {
  TState,
  TAction,
  TDispatch,
  TTranslation,
  EAction,
  NotificationsStateContext,
  NotificationsDispatchContext,
} from './Context';
import { Theme } from '@mui/material/styles';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    position: 'fixed',
    top: 0,
    right: 0,
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(2),
    zIndex: 10,
    '& > *': {
      position: 'relative !important',
      maxWidth: '300px',
      transform: 'none',
      marginTop: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
  },
}));

function notificationsReducer(state: TState, action?: TAction) {
  if (!action) {
    return state;
  }
  switch (action.type) {
    case 'add': {
      return produce(state, (draft) => {
        const payload = action.payload as TIncomeNotification;
        draft.notifications.unshift({
          ...payload,
          id: `${Date.now()}_${payload.type}`,
        });
      });
    }
    case 'remove': {
      const id = action.payload as string;
      const index = findIndex(state.notifications, { id });
      return produce(state, (draft) => {
        draft.notifications.splice(index, 1);
      });
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

interface INotificationsProps {
  translations?: TTranslation;
}
const defTranslations = { error: 'Error', info: 'Info', warning: 'Warning', success: 'Success' };

export default function Notifications({ translations = defTranslations }: INotificationsProps) {
  const [state, dispatch] = useReducer(notificationsReducer, { notifications: [], translations });
  return (
    <NotificationsStateContext.Provider value={state}>
      <NotificationsDispatchContext.Provider value={dispatch}>
        <NotificationsComponent />
      </NotificationsDispatchContext.Provider>
    </NotificationsStateContext.Provider>
  );
}

function NotificationsComponent() {
  const classes = useStyles();
  const state: TState | undefined = useContext(NotificationsStateContext);
  const dispatch: TDispatch | undefined = useContext(NotificationsDispatchContext);

  const addNotification = (notification: TIncomeNotification) => {
    dispatch && dispatch({ type: EAction.add, payload: notification });
  };

  const removeNotification = (id: string) => {
    dispatch && dispatch({ type: EAction.remove, payload: id });
  };

  useEffect(() => {
    store.register({
      addNotification,
      removeNotification,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={classes.root}>
      {state &&
        state.notifications.map((notification: INotification) => (
          <Notification
            key={notification.id}
            removeNotification={removeNotification}
            notification={notification}
          />
        ))}
    </div>
  );
}
