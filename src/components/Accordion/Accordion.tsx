import React from 'react';
import { makeStyles } from '@mui/styles';
import MUIAccordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import Typography from '@mui/material/Typography';
import { Theme } from '@mui/material/styles';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
    boxShadow: 'none',
    margin: 0,
    marginBottom: 20,
    background: 'none',
    '&:last-child': {
      marginBottom: 0,
    },
    '&.Mui-expanded': {
      margin: 0,
    },
    '&:before': {
      display: 'none',
    },
    '& .MuiAccordionSummary-root': {
      flexDirection: 'row-reverse',
      padding: 0,
      minHeight: 'auto',
    },
    '& .MuiAccordionDetails-root': {
      padding: '0 0 0 40px',
      borderLeft: `1px solid ${theme.palette.text.primary}`,
      margin: '20px 0 20px 80px',
    },
    '& .MuiAccordionSummary-expandIcon': {
      marginRight: 30,
      '& svg': {
        fontSize: theme.typography.h2.fontSize,
        color: theme.palette.text.primary,
      },
    },
    '& .MuiAccordionSummary-content': {
      margin: 0,
    },
  },
}));

interface IAccordionItems {
  title: string;
  text: string;
}

export interface IAccordionProps {
  items: IAccordionItems[];
}

function Accordion(props: IAccordionProps) {
  const { items, ...rest } = props;
  const [expanded, setExpanded] = React.useState<string | false>(false);
  const classes = useStyles();

  const handleChange = (panel: string) => (_event: React.ChangeEvent<{}>, isExpanded: boolean) => {
    setExpanded(isExpanded ? panel : false);
  };

  const accordionProps = {
    classes,
    ...rest,
  };

  if (!items.length) {
    return null;
  }

  return (
    <>
      {items.map((item) => (
        <MUIAccordion
          {...accordionProps}
          onChange={handleChange(item.title)}
          expanded={expanded === item.title}
          key={item.title}
        >
          <AccordionSummary expandIcon={expanded === item.title ? <RemoveIcon /> : <AddIcon />}>
            <Typography variant="h2">{item.title}</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography variant="body1">{item.text}</Typography>
          </AccordionDetails>
        </MUIAccordion>
      ))}
    </>
  );
}

Accordion.defaultProps = {
  items: [],
};

export default Accordion;
