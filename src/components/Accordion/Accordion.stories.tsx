import React from 'react';
import Container from '@root/.storybook/helpers/Container';
import Cell from '@root/.storybook/helpers/Cell';

import Accordion from './Accordion';

export default {
  component: Accordion,
  title: 'Accordion',
};

const accordionItems = [
  {
    title: 'Accordion item 1',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.',
  },
  {
    title: 'Accordion item 2',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.',
  },
];

export const States = () => {
  return (
    <Container>
      <Cell>
        <Accordion items={accordionItems} />
      </Cell>
    </Container>
  );
};
