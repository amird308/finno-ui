import React from 'react';
import MUISelect, { SelectProps } from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import MenuItem from '@mui/material/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import KeyboardArrowDownOutlinedIcon from '@mui/icons-material/KeyboardArrowDownOutlined';

export interface ISelectProps extends SelectProps {
  errorMessage?: React.ReactNode | string;
  label?: React.ReactNode | string;
  options?: { value: string | number; label: string | number }[];
}

function Select(props: ISelectProps) {
  const { errorMessage, label, options, ...rest } = props;

  const selectProps = {
    IconComponent: KeyboardArrowDownOutlinedIcon,
    label,
    ...rest,
  };

  function renderOptions() {
    return options?.length
      ? options.map((option) => {
          return (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          );
        })
      : null;
  }

  return (
    <FormControl variant="outlined" fullWidth error={!!errorMessage}>
      {label && <InputLabel>{label}</InputLabel>}
      <MUISelect {...selectProps}>{renderOptions()}</MUISelect>
      {errorMessage && <FormHelperText>{errorMessage}</FormHelperText>}
    </FormControl>
  );
}

Select.defaultProps = {
  value: '',
  label: '',
  errorMessage: '',
  options: [],
};

export default Select;
