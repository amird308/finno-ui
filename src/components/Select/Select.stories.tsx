import React from 'react';
import Container from '@root/.storybook/helpers/Container';
import Cell from '@root/.storybook/helpers/Cell';

import Select from './Select';

export default {
  component: Select,
  title: 'Select',
};

export const States = () => {
  const [value, setValue] = React.useState('');

  const handleChange = (event: any) => {
    setValue(event.target.value);
  };

  const options = [
    {
      value: 1,
      label: 'Option 1',
    },
    {
      value: 2,
      label: 'Option 2',
    },
    {
      value: 3,
      label: 'Option 3',
    },
  ];

  return (
    <Container>
      <Cell>
        <Select label="Select" onChange={handleChange} value={value} options={options} />
      </Cell>
      <Cell>
        <Select label="Active" value={2} options={options} />
      </Cell>
      <Cell>
        <Select label="Disabled" options={options} disabled />
      </Cell>
      <Cell>
        <Select label="Error" errorMessage="With error message" options={options} />
      </Cell>
    </Container>
  );
};
