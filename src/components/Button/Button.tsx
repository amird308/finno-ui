import React, { forwardRef } from 'react';
import makeStyles from '@mui/styles/makeStyles';
import MUIButton, { ButtonProps } from '@mui/material/Button';

import Loader from '../Loader/Loader';

export interface IButtonProps extends ButtonProps {
  loading?: boolean;
}
const useLoaderStyles = makeStyles(() => ({
  loader: {
    margin: '0 10px 0 0',
    width: 18,
    height: 18,
  },
}));

const Button = forwardRef<HTMLButtonElement, IButtonProps>((props: IButtonProps, ref) => {
  const { loading, disabled, children, ...rest } = props;
  const loaderClasses = useLoaderStyles();
  const buttonProps = {
    disabled: loading || disabled,
    ...rest,
  };
  return (
    <MUIButton ref={ref} {...buttonProps}>
      {loading && <Loader className={loaderClasses.loader} />}
      {children}
    </MUIButton>
  );
});

Button.displayName = 'Button';

Button.defaultProps = {
  color: 'primary',
  loading: false,
};

export default Button;
