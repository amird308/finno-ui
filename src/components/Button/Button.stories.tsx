import React from 'react';
import Container from '@root/.storybook/helpers/Container';
import Cell from '@root/.storybook/helpers/Cell';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Button from './Button';

export default {
  component: Button,
  title: 'Button',
};

export const States = () => {
  return (
    <Container>
      <Cell>
        <Button>standard</Button>
        <Button>default</Button>
        <Button color="primary">primary</Button>
        <Button color="secondary">secondary</Button>
        <Button disabled>disabled</Button>
      </Cell>
      <Cell>
        <Button startIcon={<ArrowBackIcon />}>startIcon</Button>
        <Button endIcon={<ArrowForwardIcon />}>endIcon</Button>
        <Button variant="contained">contained</Button>
        <Button variant="contained" loading>
          loading
        </Button>
      </Cell>
      <Cell>
        <Button variant="outlined">default outlined</Button>
        <Button variant="outlined" color="primary" loading>
          primary outlined
        </Button>
      </Cell>
      <Cell>
        <Button variant="outlined" color="secondary">
          This is long text button
        </Button>
        <Button variant="text">default text</Button>
        <Button variant="text" color="primary">
          primary text
        </Button>
        <Button variant="text" color="secondary">
          secondary text
        </Button>
      </Cell>
      <Cell>
        <Button>Danger button</Button>
        <Button>Small button</Button>
        <Button>zeroMinWidth</Button>
      </Cell>
      <Cell>
        <Button fullWidth startIcon={<ArrowBackIcon />} endIcon={<ArrowForwardIcon />}>
          fullWidth
        </Button>
      </Cell>
    </Container>
  );
};
